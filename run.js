/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import { always, pick, replace, clone } from 'ramda'
import fs from 'fs'
import pathlib from 'path'

import { ENV_MASK } from './api/configuration'

import Koa from 'koa'
import koaSend from 'koa-send'
import koaServe from 'koa-static'
import koaMount from 'koa-mount'
import koaCompress from 'koa-compress'
import koaCors from 'koa2-cors'
import koaBodyParser from 'koa-bodyparser'
import Router from 'koa-tree-router'

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import API from './api/index.js'
import applyUserConfiguration from './api/configuration.js'
import rootReducer from './store/reducers/index.js'
import rootSaga from './store/sagas/index.js'

const die = (e) => {
  if (e) {
    console.log(e.stack)
  }
  console.log('TERMINATING')
  process.exit(e ? 1 : 0)
}

process.once('SIGHUP', die)
process.once('SIGTERM', die)
process.once('SIGINT', die)
process.once('uncaughtException', die)
process.once('uncaughtRejection', die)

let configuration

if (process.env.DS_ENVIRONMENT === 'cloud') {
  configuration = pick(ENV_MASK, process.env)
} else {
  die(new Error('Reading cfg from file no longer supported'))
}

let dsCFG = applyUserConfiguration(configuration)
dsCFG.runtimes = API
API.reconfigure(dsCFG)
const deepsalter = rootSaga(API)

const sagaMiddleware = createSagaMiddleware()
const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
)
sagaMiddleware.run(deepsalter)

const baseWebPage = fs.readFileSync(pathlib.resolve('http/index.html')).toString('UTF8')

function makePage (data) {
  var out = replace('%SAVED%', data.saved, baseWebPage)
  out = replace('%REPLIED%', data.replied, out)
  out = replace('%JOURNOS%', data.journos, out)
  out = replace('%SINCE%', data.since, out)
  return out
}

let cachedData = store.getState().deepsalter
let cachedPage = makePage(store.getState().deepsalter)

function getPage (data) {
  if (data.saved !== cachedData.saved) {
    cachedData = clone(data)
    cachedPage = makePage(data)
  }
  return cachedPage
}

const port = process.env.PORT || 8080
const router = new Router()
router.get('/modlogs/latest/:subreddit/', async ctx => {
  try {
    let data = await API.reddit.getModLog(ctx.params.subreddit)
    ctx.body = {
      status: 'OK',
      logs: data
    }
  } catch (e) {
    console.log(e.stack)
    ctx.body = {
      status: 'KO',
      error: e.message
    }
  }
})
router.get('/modlogs/before/:subreddit/:before/:limit', async ctx => {
  let { subreddit, before, limit } = ctx.params
  try {
    let data = await API.reddit.getModLog(subreddit, null, before, limit)
    ctx.body = {
      status: 'OK',
      logs: data
    }
  } catch (e) {
    console.log(e.stack)
    ctx.body = {
      status: 'KO',
      error: e.message
    }
  }
})
router.get('/modlogs/after/:subreddit/:after/:limit', async ctx => {
  let { subreddit, after, limit } = ctx.params
  try {
    let data = await API.reddit.getModLog(subreddit, after, null, limit)
    ctx.body = {
      status: 'OK',
      logs: data
    }
  } catch (e) {
    console.log(e.stack)
    ctx.body = {
      status: 'KO',
      error: e.message
    }
  }
})

const app = new Koa()
app.use(async (ctx, next) => {
  const start = Date.now()
  await next()
  const ms = Date.now() - start
  ctx.set('X-Response-Time', `${ms}ms`)
  ctx.set('Access-Control-Allow-Origin', '*')
  console.log(`[${ctx.res.statusCode}] ${ctx.method} ${ctx.url} - ${ms}ms`)
})
app.use(koaCors({
  origin: always('*'),
  allowMethods: ['GET', 'POST']
}))
app.use(koaCompress())
app.use(koaBodyParser())
app.use(koaMount('/api', router.routes()))
app.use(koaMount('/modlogs', koaServe('./frontend/build')))
app.use(koaMount('/modlogs', (ctx, next) => koaSend(ctx, './frontend/build/index.html', { root: pathlib.resolve(__dirname, 'rontend') })))
app.use(async (ctx, next) => {
  ctx.body = getPage(store.getState().deepsalter)
  await next()
})
app.listen(port, () => console.log(`Frontend ready; listening on port ${port}`))
