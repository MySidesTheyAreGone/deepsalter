import React, { Fragment } from 'react'
import Markdown from 'markdown-it'
import { isNotMissing } from '../tools'

const md = new Markdown()

const Expander = props => {
  if (isNotMissing(props.original.target)) {
    return (
      <div className='expanded-target' dangerouslySetInnerHTML={{ __html: md.render(props.original.target) }} />
    )
  } else {
    return <Fragment />
  }
}

export default Expander
