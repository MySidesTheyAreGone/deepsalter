import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExternalLinkAlt, faSnowboarding, faWheelchair, faHammer, faClock, faBullseye } from '@fortawesome/free-solid-svg-icons'
import { faRedditAlien } from '@fortawesome/free-brands-svg-icons'

const iconMap = {
  link: faExternalLinkAlt,
  snowboarding: faSnowboarding,
  wheelchair: faWheelchair,
  hammer: faHammer,
  clock: faClock,
  bullseye: faBullseye,
  reddit: faRedditAlien
}

const Icon = ({ name }) => (<FontAwesomeIcon key='icon' className='icon' icon={iconMap[name]} />)

export default Icon
