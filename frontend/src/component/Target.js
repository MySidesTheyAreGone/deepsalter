import React from 'react'
import { take } from 'ramda'

const Target = props => {
  let value = props.column.accessor(props.original)
  if (value && value.length > 105) {
    value = take(100, value) + ' [...]'
  }
  return (
    <div>{value}</div>
  )
}

export default Target
