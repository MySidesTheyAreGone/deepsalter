import React, { Fragment } from 'react'
import Icon from './Icon'

const Header = ({ icon, text }) => {
  let iconElm = (<Icon name={icon} />)
  if (text == null || text === '') {
    return (<Fragment>{iconElm}</Fragment>)
  } else {
    return (<Fragment>{iconElm}<span key='text'>&nbsp;{text}</span></Fragment>)
  }
}

export default Header
