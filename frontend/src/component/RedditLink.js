import React, { Fragment } from 'react'
import Icon from './Icon'
import { isNil } from 'ramda'

const RedditLink = ({ url }) => {
  if (isNil(url)) {
    return <Fragment />
  } else {
    return (
      <div className='redditLink'>
        <a href={`https://reddit.com${url}`} className='link' target='_blank' rel='noopener noreferrer'>
          <Icon name='link' />
        </a>
      </div>
    )
  }
}

export default RedditLink
