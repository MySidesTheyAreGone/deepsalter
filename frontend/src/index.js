import React from 'react'
import ReactDOM from 'react-dom'
import App from './container/App'
import * as serviceWorker from './serviceWorker'
import { fromJS, List } from 'immutable'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { put, take, takeEvery, takeLatest, select, call, fork } from 'redux-saga/effects'
import * as acts from './actions'
import { Provider } from 'react-redux'
import { test, pipe, pick, map, defaultTo, invoker, curry, evolve, multiply, omit, mergeAll, assoc } from 'ramda'
import { parse, stringify } from 'query-string'
import { copyProp, isNotMissing, decodeWith, isMissing } from './tools'
import axioslib from 'axios'
import moment from 'moment-timezone'

const enabledFields = [
  'created_utc',
  'mod',
  'action',
  'target_author',
  'target',
  'link',
  'id'
]

const actionMap = {
  'approvelink': 'Approve link',
  'spamlink': 'Mark link as spam',
  'removelink': 'Remove link',
  'approvecomment': 'Approve comment',
  'removecomment': 'Remove comment',
  'distinguish': 'Distinguish',
  'sticky': 'Sticky',
  'muteuser': 'Mute user',
  'unmuteuser': 'Unmute user',
  'editflair': 'Edit flair',
  'wikirevise': 'Revise wiki',
  'banuser': 'Ban user',
  'unbanuser': 'Unban user',
  'spamcomment': 'Mark comment as spam',
  'unignorereports': 'Un-ignore reports',
  'ignorereports': 'Ignore reports',
  'unsticky': 'Un-sticky'
}

let axiosopts = {
  responseType: 'json',
  crossdomain: true
}
if (process.env.NODE_ENV !== 'development') {
  axiosopts.baseURL = 'http://deepsalter.mybluemix.net'
} else {
  axiosopts.baseURL = 'http://localhost:8181'
}
const axios = axioslib.create(axiosopts)

const tz = Intl.DateTimeFormat().resolvedOptions().timeZone

const dateFormat = pipe(
  multiply(1000),
  moment,
  invoker(1, 'tz')(tz),
  invoker(1, 'format')('YYYY-MM-DD HH:mm:ss')
)

const preprocess = pipe(
  d => {
    d.target = d.target_title ? `**${d.target_title}**  \n\n${d.target_body || ''}` : d.target_body
    return d
  },
  copyProp('target_permalink', 'link'),
  evolve({
    'created_utc': dateFormat,
    'action': decodeWith(actionMap)
  }),
  pick(enabledFields)
)

function applyFilterSet (filterSet, data) {
  for (let userfilter of filterSet) {
    let exp
    try {
      exp = new RegExp(userfilter.value, 'i')
    } catch (e) {
      exp = new RegExp(userfilter.value.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&'), 'i')
    }
    data = data.filter(m => test(exp, m.get(userfilter.id)))
  }
  return data
}

async function getLogsBefore (subreddit, dataIndex) {
  let res = await axios({
    method: 'GET',
    url: `/api/modlogs/before/${subreddit}/${dataIndex}/1`
  })
  if (res.data.status === 'KO') {
    throw new Error(res.data.error)
  }
  return fromJS(map(preprocess, res.data.logs))
}

async function getLogsAfter (subreddit, dataIndex) {
  let res = await axios({
    method: 'GET',
    url: `/api/modlogs/after/${subreddit}/${dataIndex}/500`
  })
  if (res.data.status === 'KO') {
    throw new Error(res.data.error)
  }
  return fromJS(map(preprocess, res.data.logs))
}

const query = parse(window.location.search)

let filterMap = omit(['s', 'i', 'n'], query)
let initialFilterSet = []

for (let fkey in filterMap) {
  initialFilterSet.push({
    id: fkey,
    value: filterMap[fkey]
  })
}

let subreddit = isMissing(query.s) ? 'KotakuInAction' : query.s
let initDataIndex = defaultTo(null, query.i)
let initialDataSize = defaultTo(500, query.n)

const initialState = fromJS({
  subreddit,
  help: false,
  loading: true,
  table: {
    data: [],
    fetched: [],
    columns: []
  },
  initDataIndex,
  dataIndex: initDataIndex,
  filterSet: [],
  page: 0,
  pageSize: 100,
  initialDataSize,
  initialFilterSet,
  hydrated: false
})

const setter = invoker(2, 'set')
const setFromProp = curry((property, state, action) => state.set(property, action[property]))

let reducer = {}
reducer[acts.UPDATE_TABLE_DATA] = (state, { logs, dataIndex }) => {
  return state.withMutations(s => {
    let fetched = s.getIn(['table', 'fetched']).concat(logs)
    let filterSet = s.get('filterSet')
    let filtered = applyFilterSet(filterSet.toJS(), fetched)
    s.setIn(['table', 'fetched'], fetched)
    s.setIn(['table', 'data'], filtered)
    if (s.getIn(['table', 'columns']).isEmpty()) {
      s.setIn(['table', 'columns'], logs.get(0))
    }
    s.set('dataIndex', dataIndex)
  })
}
reducer[acts.RESET_TABLE] = state => {
  return state.withMutations(s => {
    s.setIn(['table', 'fetched'], List())
    s.setIn(['table', 'data'], List())
    s.set('initialFilterSet', List())
    s.set('dataIndex', null)
    s.set('hydrated', false)
    s.set('initialDataSize', 500)
    s.set('initDataIndex', null)
  })
}
reducer[acts.SET_HYDRATED] = setter('hydrated', true)
reducer[acts.SET_LOADING] = setter('loading', true)
reducer[acts.UNSET_LOADING] = setter('loading', false)
reducer[acts.OPEN_HELP_POPUP] = setter('help', true)
reducer[acts.CLOSE_HELP_POPUP] = setter('help', false)
reducer[acts.SET_PAGE] = setFromProp('page')
reducer[acts.SET_PAGESIZE] = setFromProp('pageSize')
reducer[acts.SET_SHARING_QS] = setFromProp('sharingQueryString')
reducer[acts.SET_FILTERSET] = (state, { filterSet }) => state.set('filterSet', fromJS(filterSet))
reducer[acts.SET_INIT_INDEX] = setFromProp('initDataIndex')
reducer[acts.SET_TABLE_DATA] = (state, { data }) => state.setIn(['table', 'data'], data)

const rootReducer = (state = initialState, action) => {
  if (reducer[action.type] == null) {
    return state
  } else {
    return reducer[action.type](state, action)
  }
}

function * fetchDataSaga (force = false) {
  let { subreddit, initDataIndex, dataIndex, size, page, pageSize, initialDataSize, initialFilterSet, hydrated } = yield select(s => ({
    subreddit: s.get('subreddit'),
    initDataIndex: s.get('initDataIndex'),
    dataIndex: s.get('dataIndex'),
    size: s.getIn(['table', 'fetched']).size,
    initialDataSize: s.get('initialDataSize'),
    page: s.get('page'),
    pageSize: s.get('pageSize'),
    initialFilterSet: s.get('initialFilterSet'),
    hydrated: s.get('hydrated')
  }))
  let data = List()
  if (!hydrated && isNotMissing(initDataIndex)) {
    try {
      let logs = yield call(getLogsBefore, subreddit, initDataIndex)
      data = data.concat(logs)
      dataIndex = logs.get(-1).get('id')
    } catch (e) {
      console.log(e)
    }
  }
  let maxPage = Math.ceil(size / pageSize) - 1
  if (force || page >= maxPage || (!hydrated && size < initialDataSize)) {
    yield put({ type: acts.SET_LOADING })
    let notHydratedYet = (!hydrated && size < initialDataSize)
    let notEnoughRows = true
    for (let count = 0; force || notEnoughRows || notHydratedYet; ++count) {
      let logs
      try {
        logs = yield call(getLogsAfter, subreddit, defaultTo('', dataIndex))
      } catch (e) {
        console.log(e)
        break
      }
      dataIndex = logs.get(-1).get('id')
      data = data.concat(logs)
      size += logs.size
      maxPage = Math.ceil(size / pageSize) - 1
      force = false
      if (isMissing(initDataIndex)) {
        initDataIndex = logs.get(1).get('id')
        yield put({ type: acts.SET_INIT_INDEX, initDataIndex })
      }
      notEnoughRows = (count < 4 && page >= maxPage)
      notHydratedYet = (!hydrated && size < initialDataSize)
    }
    yield put({ type: acts.UNSET_LOADING })
    if (!hydrated) {
      yield put({ type: acts.SET_HYDRATED })
      yield put({ type: acts.SET_FILTERSET, filterSet: initialFilterSet.toJS() })
    }
  }
  yield put({ type: acts.UPDATE_TABLE_DATA, logs: data, dataIndex })
}

function * triggerFetchSaga () {
  yield takeEvery([acts.TRIGGER_FETCH_MORE_DATA, acts.RESET_TABLE], fetchDataSaga, [true])
}

function * pagingSaga () {
  while (true) {
    yield take([acts.SET_PAGE, acts.SET_PAGESIZE])
    yield fork(fetchDataSaga)
  }
}

function * filteringSaga (action) {
  yield call(() => new Promise(resolve => setTimeout(resolve, 400)))
  let { filterSet } = action
  let { fetchedData, subreddit } = yield select(s => ({
    fetchedData: s.getIn(['table', 'fetched']),
    subreddit: s.get('subreddit')
  }))
  let data = applyFilterSet(filterSet, fetchedData)
  if (data.size !== 0) {
    let filteredDataIndex = data.get(0).get('id')
    let sharingQueryString = `?s=${subreddit}&n=${fetchedData.size}&i=${filteredDataIndex}`
    if (isNotMissing(filterSet)) {
      let fqs = pipe(map(f => assoc(f.id, f.value, {})), mergeAll, stringify)(filterSet)
      sharingQueryString += `&${fqs}`
    }
    yield put({ type: acts.SET_SHARING_QS, sharingQueryString })
  } else {
    yield put({ type: acts.SET_SHARING_QS, sharingQueryString: null })
  }
  yield put({ type: acts.SET_TABLE_DATA, data })
}

function * filteringBufferSaga () {
  yield takeLatest(acts.SET_FILTERSET, filteringSaga)
}

function * modlogSaga () {
  yield fork(triggerFetchSaga)
  yield fork(pagingSaga)
  yield fork(filteringBufferSaga)
}

const sagaMiddleware = createSagaMiddleware()

const store = createStore(
  rootReducer,
  applyMiddleware(sagaMiddleware)
)

sagaMiddleware.run(modlogSaga)

ReactDOM.render(
  <Provider store={store}>
    <React.StrictMode>
      <App />
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
