import 'react-table/react-table.css'
import './App.css'
import * as acts from '../actions'
import { __, pipe, keys, map, assoc, omit, nthArg, evolve, ifElse, propEq, identity, merge, reject, isNil } from 'ramda'
import { immutableToJS, copyProp, decodeWith, isNotMissing } from '../tools'
import { List } from 'immutable'
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import ReactTable, { ReactTableDefaults } from 'react-table'
import Header from '../component/Header'
import RedditLink from '../component/RedditLink'
import Target from '../component/Target'
import Expander from '../component/Expander'

const headerMap = {
  'created_utc': (<Header icon='clock' text='When' />),
  'mod': (<Header icon='snowboarding' text='Moderator' />),
  'action': (<Header icon='hammer' text='Action' />),
  'target_author': (<Header icon='wheelchair' text='Victim' />),
  'target': (<Header icon='bullseye' text='Target' />),
  'link': (<Header icon='reddit' />)
}

const buildColumns = pipe(
  keys,
  map(pipe(
    assoc('accessor', __, ReactTableDefaults.column),
    copyProp('accessor', 'Header'),
    assoc('sortable', true),
    assoc('resizable', true),
    assoc('filterable', true),
    assoc('filterAll', true),
    assoc('filterMethod', nthArg(1)), // turn off native filtering
    assoc('maxWidth', 200),
    evolve({
      'Header': decodeWith(headerMap)
    }),
    ifElse(
      propEq('accessor', 'target'),
      pipe(
        assoc('Cell', Target),
        omit(['maxWidth'])
      ),
      identity
    ),
    ifElse(
      propEq('accessor', 'link'),
      merge(__, {
        'Cell': props => (<RedditLink url={props.value} />),
        'sortable': false,
        'resizable': false,
        'filterable': false,
        'width': 36
      }),
      identity
    )
  )),
  reject(propEq('accessor', 'id'))
)

class App extends Component {
  constructor () {
    super()
    this.cachedColumns = []
    this.cachedRawColumns = List()
  }

  componentDidMount () {
    this.props.dFetchData()
    if (isNotMissing(this.props.initialFilterSet)) {
      this.initFilters = this.props.initialFilterSet.toJS()
    }
  }

  resetTable () {
    this.initFilters = []
    this.props.dResetTable()
  }

  render () {
    let { loading, columns, sharingQueryString } = this.props
    if (this.cachedRawColumns !== columns) {
      this.cachedRawColumns = columns
      this.cachedColumns = buildColumns(columns.toJS())
    }
    let statusLeft = (
      <Fragment>
        <span id='logo' className='active' onClick={this.props.dFetchData}>{loading ? '(ﾉ≧∇≦)ﾉ ﾐ ✿' : '(◡‿◡✿)'}&nbsp;</span>
        <div id='message'>{loading ? '!!! Loading !!!' : 'zzZZzzZZzzZZ'}</div>
      </Fragment>
    )
    let share
    if (isNotMissing(sharingQueryString)) {
      share = (<a href={sharingQueryString}>Current table permalink</a>)
    }
    let tableProps = {
      className: '-striped',
      columns: this.cachedColumns,
      data: this.props.data,
      resolveData: immutableToJS,
      minRows: 0,
      defaultPageSize: 100,
      pageSizeOptions: [100, 200, 500, 1000],
      loading: loading,
      SubComponent: Expander,
      onFilteredChange: this.props.dChangeFilterset,
      onPageChange: this.props.dChangePage,
      onPageSizeChange: this.props.dChangePageSize
    }
    if (!isNil(this.initFilters)) {
      tableProps.filtered = this.initFilters
      this.initFilters = null
    }
    const $ = ' | '
    return (
      <div className='App'>
        <ReactTable {...tableProps} />
        <div id='status'>
          {statusLeft}
          <div id='progress'>
            {share}{$}
            <span className='help active' onClick={this.props.dOpenHelpPopup}>HELP!!</span>{$}
            {this.props.subreddit} modlogs{$}
            <span className='active' onClick={this.props.dFetchData}>{this.props.sizeFetched} rows fetched</span>{$}
            <span className='reset active' onClick={() => this.resetTable()}>RESET</span>
          </div>
        </div>
        <div id='help' style={{ display: this.props.help ? 'block' : 'none' }} onClick={this.props.dCloseHelpPopup}>
          <h1>(ﾉ≧∇≦)ﾉ ﾐ <span className='help'>✚</span> &nbsp; Here's some help!!!</h1>
          <p>If you manipulate the table in any way, a "Current table permalink" link will appear to the left of the "HELP!!" button. Use that link to share the table you're looking at, complete with filters and everything. The link changes automatically as you keep filtering or navigating around.</p>
          <p>Click on any header to sort by that column; keep SHIFT pressed as you keep clicking columns to sort by all of them in the order in which you clicked them (e.g. keep SHIFT pressed and try clicking on "Moderator" first and then "Victim"; then do it again in the opposite order).</p>
          <p>Type in any text box under a header to filter by what you type. If you type a valid regular expression, your input will be treated as such.</p>
          <p>If Salt-chan is sleeping in the bottom left corner, click on (◡‿◡✿) to wake her up and fetch 500 more rows from Reddit. You're told how many rows you have in the bottom right corner.</p>
          <p>Scroll the table all the way to its bottom to access pagination controls. If you navigate to the last page, Salt-chan will wake up and fetch enough rows to display at least one more page after that one.</p>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    loading: state.get('loading'),
    columns: state.getIn(['table', 'columns']),
    data: state.getIn(['table', 'data']),
    sizeFetched: state.getIn(['table', 'fetched']).size,
    sharingQueryString: state.get('sharingQueryString'),
    help: state.get('help'),
    initialFilterSet: state.get('initialFilterSet'),
    subreddit: state.get('subreddit')
  }
}

const mapDispatchToProps = dispatch => ({
  dFetchData: () => dispatch({ type: acts.TRIGGER_FETCH_MORE_DATA }),
  dChangeFilterset: filterSet => dispatch({ type: acts.SET_FILTERSET, filterSet }),
  dChangePage: page => dispatch({ type: acts.SET_PAGE, page }),
  dChangePageSize: pageSize => dispatch({ type: acts.SET_PAGESIZE, pageSize }),
  dOpenHelpPopup: () => dispatch({ type: acts.OPEN_HELP_POPUP }),
  dCloseHelpPopup: () => dispatch({ type: acts.CLOSE_HELP_POPUP }),
  dResetTable: () => dispatch({ type: acts.RESET_TABLE })
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
