import { curry, clone, defaultTo, invoker, either, isNil, isEmpty, pipe, not } from 'ramda'

export const copyProp = curry((source, dest, obj) => {
  let c = clone(obj)
  c[dest] = c[source]
  return c
})

export const decodeWith = curry((mapobj, propName) => defaultTo(propName, mapobj[propName]))

export const immutableToJS = invoker(0, 'toJS')

export const isMissing = either(isNil, isEmpty)

export const isNotMissing = pipe(isMissing, not)

export const trace = x => {
  console.log(x)
  return x
}
