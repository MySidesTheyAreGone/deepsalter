/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

*/

import R from 'ramda'

const decideBy = R.curry((fnName, obj) => obj[fnName]())

let CFG = {
  budget: 0,
  budgetDuration: 0
}

let running = []
let queue = []
let budget = 0

function selfAwarePromise (p) {
  let pending = true
  p.then(() => (pending = false)).catch(() => (pending = false))
  p.isPending = () => pending
  return p
}

function interval (delay) {
  return new Promise((resolve) => {
    setTimeout(resolve, delay)
  })
}

async function budgetLoop () {
  if (CFG.budgetDuration === 0) return
  await interval(CFG.budgetDuration)
  budget = CFG.budget
  setTimeout(advance, 1)
  setTimeout(budgetLoop, 1)
}

export function enqueue (fn, params) {
  var resolveAction, rejectAction
  var p = new Promise((resolve, reject) => {
    resolveAction = resolve
    rejectAction = reject
  })

  queue.push({
    act: fn,
    params: params,
    res: resolveAction,
    rej: rejectAction
  })

  setTimeout(advance, 1)
  return p
}

async function run (delay, p) {
  await Promise.all([
    R.apply(p.act, p.params).then(p.res).catch(p.rej),
    interval(delay)
  ])
  setTimeout(advance, 1)
}

function advance () {
  let p
  running = R.filter(decideBy('isPending'), running)
  while (running.length < CFG.concurrency && queue.length > 0 && budget > 0) {
    p = queue.shift()
    budget--
    running.push(selfAwarePromise(run(CFG.delay, p)))
  }
}

export function reconfigure (cfg) {
  CFG = {
    concurrency: cfg.concurrency,
    delay: cfg.delay,
    budget: cfg.budget,
    budgetDuration: cfg.budgetDuration
  }
  budget = CFG.budget
  budgetLoop()
}

export const API = { enqueue, reconfigure }

export default API
