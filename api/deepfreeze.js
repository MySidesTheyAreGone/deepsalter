/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import { decodeHTML } from 'entities'

let CFG
let W

export function reconfigure (cfg) {
  CFG = {
    endpoint: cfg.deepfreeze.endpoint,
    journoPageBaseURL: cfg.deepfreeze.journoPageBaseURL,
    TTL: cfg.deepfreeze.TTL
  }
  W = cfg.runtimes.web
}

function adjustDeepfreezeEntry (j) {
  let journo = R.clone(j)
  try {
    journo.entries = parseInt(journo.entries)
  } catch (e) {
    journo.entries = null
  }
  let jname = decodeHTML(journo.name)
  if (journo.searchTerm == null) {
    journo.searchTerm = R.toLower(jname)
    journo.url = CFG.journoPageBaseURL + encodeURIComponent(R.replace(/ /g, '_', journo.searchTerm))
  }
  journo.name = jname + (journo.entries === 0 ? ' [no entries]' : '')
  return journo
}

async function fetch () {
  let upstreamDB = await W.requestJSON(CFG.endpoint)
  let deepfreezeDB = R.map(adjustDeepfreezeEntry, upstreamDB)
  return { db: deepfreezeDB, TTL: CFG.TTL }
}

export const API = { reconfigure, fetch }

export default API
