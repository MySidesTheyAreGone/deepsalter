/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import CF from './cfsolver.js'
import cookielib from 'tough-cookie'
import cheerio from 'cheerio'
import got from 'got'
import Mercury from '@postlight/mercury-parser'

import _matchers from '../data/matchers.json'
import _guessers from '../data/guessers.json'

let CFG = {}

export function reconfigure (cfg) {
  CFG = {
    userAgent: cfg.scraper.userAgent,
    maxDownloadSize: cfg.scraper.maxDownloadSize,
    log: cfg.runtimes.system.log
  }
}

const _emptyresponse = { headers: { 'content-type': 'text/html' }, body: '<html><head></head><body></body></html>' }
const _httpreg = 'https?:\/\/[a-zA-Z0-9]*\.?' // eslint-disable-line

const clean = R.pipe(
  R.replace(/\r?\n|\r/g, ' '),
  R.trim,
  R.replace(/\s+/g, ' ')
)

const isMissing = R.either(R.isNil, R.isEmpty)

function toMegabytes (bytes) {
  return (bytes / (1024 * 1024)).toFixed(2) + 'MB'
}

const interval = (n) => new Promise((resolve) => setTimeout(resolve, n * 1000))

function updateCookies (headers, options) {
  let cookies = []
  if (!isMissing(headers['set-cookie'])) {
    if (R.is(Array, headers['set-cookie'])) {
      cookies = R.map(cookielib.parse, headers['set-cookie'])
    } else {
      cookies = R.of(cookielib.parse(headers['set-cookie']))
    }
    // tough-cookie might return null (??)
    cookies = R.reject(isMissing, cookies)
    let newCookies = R.join('; ', R.map(R.invoker(0, 'cookieString'), cookies))
    if (isMissing(options.headers.cookie)) {
      options.headers.cookie = newCookies
    } else {
      options.headers.cookie += '; ' + newCookies
    }
  }
}

function limitedRequest (url, opts) {
  let options = R.clone(opts)
  let limit = Infinity
  if (!R.isNil(CFG.maxDownloadSize)) {
    limit = CFG.maxDownloadSize
  }
  return new Promise((resolve, reject) => {
    let settled = false
    let length = 0
    let request
    let headers
    let parseable = true
    let cloudflare = false
    let body = ''
    let err
    got.stream(url, options)
      .on('request', r => {
        request = r
      })
      .on('response', (resp) => {
        if (settled) return
        let hasNoContentType = R.isNil(resp.headers['content-type'])
        let isNotHTML = resp.headers['content-type'].indexOf('text/html') === -1
        let isNotXHTML = resp.headers['content-type'].indexOf('application/xhtml') === -1
        if (hasNoContentType || (isNotHTML && isNotXHTML)) {
          settled = true
          parseable = false
          request.abort()
          resolve(R.merge({ options, cloudflare, parseable }, _emptyresponse))
        }
        headers = resp.headers
      })
      .on('data', (chunk) => {
        if (settled) return
        length += chunk.length
        if (length > limit) {
          settled = true
          request.abort()
          resolve(R.merge({ options, cloudflare, parseable }, _emptyresponse))
          return
        }
        body += chunk.toString('utf8')
      })
      .on('redirect', (r, o) => {
        if (settled) return
        headers = r.headers
        o.url = o.href
        updateCookies(headers, o)
        options = R.clone(o)
        cloudflare = false
      })
      .on('error', (e) => {
        if (settled) return
        err = e
        headers = e.headers
        if (!isMissing(e.headers) && e.headers.server === 'cloudflare-nginx') {
          cloudflare = true
        } else {
          reject(e)
        }
      })
      .on('end', () => {
        if (settled) return
        CFG.log('info', toMegabytes(length) + ' downloaded from ' + url)
        settled = true
        if (!isMissing(err) && !cloudflare) {
          reject(err)
          return
        }
        updateCookies(headers, options)
        resolve({ body, options, cloudflare, parseable })
      })
  })
}

export async function rawRequest (url, opts) {
  let response = await got(url, opts)
  return response.body
}

export async function requestJSON (url) {
  let response = await got(url, { json: true })
  return response.body
}

export async function requestWebpage (url, options = false) {
  let opts = options || {
    url: url,
    retries: 0,
    decompress: true,
    headers: { 'User-Agent': CFG.scraperUserAgent },
    followRedirect: true
  }
  let response = await limitedRequest(opts.url, opts)
  if (response.cloudflare) {
    let deprotection = CF.deprotect(response.options, response)
    if (deprotection.action === 'done') {
      return { body: response.body, parseable: response.parseable }
    } else if (deprotection.action === 'delayed request') {
      await interval(CF.cloudflareDelay)
      return requestWebpage(url, deprotection.payload)
    } else if (deprotection.action === 'request') {
      return requestWebpage(url, deprotection.payload)
    }
  } else {
    return { body: response.body, parseable: response.parseable }
  }
}

const scrapeHTML = R.curry((selector, attr, body) => {
  if (R.isNil(body)) {
    return null
  }
  let jq
  if (R.isNil(body.html) || !R.is(Function, body.html)) {
    jq = cheerio.load(body)
  } else {
    jq = body
  }
  let out
  if (attr === 'text') {
    out = jq(selector).map(function () {
      return jq(this).text()
    }).get()
  } else {
    out = jq(selector).map(function () {
      return jq(this).attr(attr)
    }).get()
  }
  return out
})

let compiledMatchers = []
for (let matcher of _matchers) {
  compiledMatchers.push({
    site: new RegExp(_httpreg + matcher.site, 'i'),
    query: scrapeHTML(matcher.query, R.defaultTo('text', matcher.attribute))
  })
}

const findAuthorPrecisely = R.curry((url, body) => {
  let matchers = R.filter(R.propSatisfies((v) => R.test(v, url), 'site'), compiledMatchers)
  if (R.length(matchers) > 0) {
    let authors = R.pipe(
      R.map(R.pipe(
        R.invoker(1, 'query')(body),
        R.map(clean),
        R.reject(isMissing)
      )),
      R.reject(isMissing),
      R.reduce(R.concat, [])
    )(matchers)
    return R.join(' ', authors)
  } else {
    return ''
  }
})

function guessAuthor (body) {
  let jq = cheerio.load(body)
  let author
  for (let guesser of _guessers) {
    author = R.head(scrapeHTML(guesser[0], guesser[1], jq))
    if (!R.isNil(author) && !isMissing(clean(author))) {
      return clean(author)
    }
  }
  return ''
}

const unarchiveOriginal = R.pipe(
  scrapeHTML('td:contains(Original) ~ td input', 'value'),
  R.head
)

const unarchiveFrom = R.pipe(
  scrapeHTML('td:contains(Saved from) ~ td input[type=text]', 'value'),
  R.head
)

const unwayback = R.pipe(
  scrapeHTML('#wmtbURL', 'value'),
  R.head,
  R.defaultTo('')
)

const ununvisit = R.pipe(
  scrapeHTML('a.perma', 'text'),
  R.head,
  R.defaultTo('')
)

const ungoogle = R.pipe(
  scrapeHTML('base', 'href'),
  R.head,
  R.defaultTo('')
)

async function mercuryParse (url) {
  let parsedData = await Mercury.parse(url)
  let out = R.pick(['title', 'content'], parsedData)
  out.url = url
  return out
}

const isBlacklisted = R.anyPass([
  R.equals(''),
  R.test(/^https?:\/\/(www\.)?deepfreeze\.it/i),
  R.test(/^https?:\/\/(www\.)?(youtube.com|youtu.be)/i),
  R.test(/^https?:\/\/(www\.)?twitter.com/i),
  R.test(/imgur/i),
  R.test(/^https?:\/\/(www\.|i\.)?redd\.?it/i),
  R.test(/(\.gif|\.jpg|\.png|\.pdf)/i)
])

export async function scrape (url, origin) {
  let response
  if (isBlacklisted(url)) {
    response = R.merge({ parseable: false }, _emptyresponse)
  } else {
    response = await requestWebpage(url)
  }
  let body = response.body
  if (!response.parseable) {
    return { author: '', title: '', content: '' }
  } else if (R.test(/^https?:\/\/archive\.(is|fo|li|today|vn|md|ph)/i, url)) {
    CFG.log('info', `Unarchiving ${url}`)
    let unarchived = unarchiveOriginal(body)
    if (R.either(R.isNil, R.isEmpty)(unarchived)) {
      unarchived = R.defaultTo('', unarchiveFrom(body))
    }
    if (unarchived === '') {
      throw new Error('Could not extract the original URL from archive.is (' + url + '); this is a bug. Giving up.')
    } else {
      return scrape(unarchived, R.defaultTo(url, origin))
    }
  } else if (R.test(/^https?:\/\/web\.archive\.org/i, url)) {
    CFG.log('info', `Un-waybacking ${url}`)
    const unwaybacked = unwayback(body)
    if (unwaybacked === '') {
      throw new Error('Could not extract the original URL from the Wayback Machine (' + url + '); this is a bug. Giving up.')
    } else {
      return scrape(unwaybacked, R.defaultTo(url, origin))
    }
  } else if (R.test(/^https?:\/\/unvis\.it/i, url)) {
    CFG.log('info', `Un-unvis.it-ing ${url}`)
    const ununvisited = ununvisit(body)
    if (ununvisited === '') {
      throw new Error('Could not extract the original URL from unvis.it (' + url + '); this is a bug. Giving up.')
    } else {
      return scrape(ununvisited, R.defaultTo(url, origin))
    }
  } else if (R.test(/^https?:\/\/webcache\.googleusercontent\.com/i, url)) {
    CFG.log('info', `Un-googlecaching ${url}`)
    const ungoogled = ungoogle(body)
    if (ungoogled === '') {
      throw new Error('Could not extract the original URL from Google Cache (' + url + '); this is a bug. Giving up.')
    } else {
      return scrape(ungoogled, R.defaultTo(url, origin))
    }
  } else {
    let parsedContent = {}
    CFG.log('info', `Mercury-parsing ${url}`)
    parsedContent = await mercuryParse(url)
    parsedContent.author = findAuthorPrecisely(url, body)
    if (isMissing(parsedContent.author)) {
      parsedContent.author = guessAuthor(body)
    }
    parsedContent.origin = R.defaultTo(url, origin)
    return parsedContent
  }
}

export const API = { reconfigure, rawRequest, requestJSON, requestWebpage, scrape }

export default API
