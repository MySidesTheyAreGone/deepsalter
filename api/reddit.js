/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import { merge, clone, isNil, is, pipe, path, pluck, map, uniq, assoc, flatten, pick, curry, defaultTo, not, match, prop, split, reject, ifElse, propEq, isEmpty, zipObj, props } from 'ramda'
import moment from 'moment'
import urllib from 'url'

const _urlExp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/ig

let CFG
let W

export function reconfigure (cfg) {
  CFG = clone(cfg.reddit)
  W = cfg.runtimes.web
}

const getApiUrl = (path) => urllib.parse(CFG.apiBaseUrl + path)

const getRequest = (userAgent, accessToken, path, method) => merge(getApiUrl(path), {
  method,
  headers: {
    'Authorization': 'Bearer ' + accessToken,
    'User-Agent': userAgent
  }
})

function processRedditResponse (x) {
  if (!is(Object, x)) {
    throw new Error('Reddit returned an invalid response.')
  } else if (x.error == null) {
    return x
  } else {
    throw new Error(x.message || x.error)
  }
}

function extractListingData (l) {
  return pipe(
    path(['data', 'children']),
    pluck('data'),
    map(d => {
      d.externalResources = uniq(getSubmittedUrls(d))
      return d
    })
  )(l)
}

const processRedditListing = pipe(
  processRedditResponse,
  extractListingData
)

export async function getAuthToken () {
  let request = merge(urllib.parse(CFG.authUrl), {
    method: 'POST',
    headers: {
      'User-Agent': CFG.userAgent
    }
  })
  let options = {
    json: true,
    form: true,
    body: {
      grant_type: 'password',
      username: CFG.username,
      password: CFG.password
    },
    auth: CFG.clientId + ':' + CFG.clientSecret
  }
  let response = processRedditResponse(await W.rawRequest(request, options))
  return assoc('expirationDate', moment().add(CFG.tokenExpiry, 'minutes'), response)
}

export async function getPost (tokenData, id) {
  const request = getRequest(CFG.userAgent, tokenData.access_token, 'by_id/t3_' + id, 'GET')
  const options = {
    json: true,
    query: {
      raw_json: 1,
      show: 'all',
      count: 0
    }
  }
  let response = await W.rawRequest(request, options)
  return processRedditListing(response)
}

export async function getNewPosts (tokenData) {
  let responses = []
  for (let subreddit of CFG.subreddits) {
    const request = getRequest(CFG.userAgent, tokenData.access_token, 'r/' + subreddit + '/new', 'GET')
    const options = {
      json: true,
      query: {
        raw_json: 1,
        show: 'all',
        count: 0,
        limit: CFG.limit
      }
    }
    responses.push(processRedditListing(await W.rawRequest(request, options)))
  }
  return flatten(responses)
}

export async function savePost (tokenData, post) {
  const request = getRequest(CFG.userAgent, tokenData.access_token, 'api/save', 'POST')
  const options = {
    json: true,
    form: true,
    body: {
      id: post.name
    }
  }
  let res = await W.rawRequest(request, options)
  processRedditResponse(res)
  return post
}

export async function replyToThing (tokenData, post) {
  const request = getRequest(CFG.userAgent, tokenData.access_token, 'api/comment', 'POST')
  const options = {
    json: true,
    form: true,
    body: {
      api_type: 'json',
      text: post.replyText,
      thing_id: post.name
    }
  }
  let res = await W.rawRequest(request, options)
  processRedditResponse(res)
  return post
}

const enabledModlogFields = [
  'id',
  'created_utc',
  'mod',
  'target_author',
  'target_title',
  'target_body',
  'target_permalink',
  'action'
]

export async function getModLog (subreddit, after, before, limit = 1000) {
  const modLogURL = `https://www.reddit.com/r/${subreddit}/about/log/.json`
  let request = merge(urllib.parse(modLogURL), {
    method: 'GET',
    headers: { 'User-Agent': CFG.userAgent }
  })
  let query = {
    feed: '7e9b27126097f51ae6c9cd5b049af34891da6ba6',
    user: 'publicmodlogs',
    limit
  }
  if (!isNil(after)) {
    query.after = after
  }
  if (!isNil(before)) {
    query.before = before
  }
  let options = {
    json: true,
    query
  }
  let res = await W.rawRequest(request, options)
  res = processRedditListing(res)
  return map(pick(enabledModlogFields), res)
}

// https://reddit.com/comments/${post}//${comment}
export const deconstructPermalink = pipe(
  split('/'),
  reject(isEmpty),
  ifElse(
    propEq('length', 5),
    props([3]),
    props([3, 5])
  ),
  zipObj(['post', 'comment'])
)

const decideByProp = curry((property, obj) => pipe(prop(property), defaultTo(false))(obj))
const decideByNotProp = curry((property, obj) => pipe(decideByProp(property), not)(obj))

export const postIsSaved = decideByProp('saved')
export const postIsNotSaved = decideByNotProp('saved')
export const postIsSelf = decideByProp('is_self')
export const postIsNotSelf = decideByNotProp('is_self')

export function getSubmittedUrls (post) {
  if (postIsSelf(post)) {
    return match(_urlExp, post.selftext)
  } else {
    return [pipe(prop('url'), defaultTo(''))(post)]
  }
}

export const api = {
  reconfigure,
  getAuthToken,
  getPost,
  getNewPosts,
  savePost,
  replyToThing,
  postIsSaved,
  postIsNotSaved,
  postIsSelf,
  postIsNotSelf,
  getSubmittedUrls,
  getModLog
}

export default api
