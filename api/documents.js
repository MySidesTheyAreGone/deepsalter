/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import pathlib from 'path'
import urllib from 'url'
import fs from 'fs'
import R from 'ramda'
import mustache from 'mustache'

import cancer from '../data/cancer.json'
import quips from '../data/quips.json'

const replyTemplate = fs.readFileSync(pathlib.resolve('data/reply.mustache')).toString()

const searchFields = {
  post: ['title', 'selftext'],
  webpage: ['author', 'title', 'content']
}

let CFG

export function reconfigure (cfg) {
  CFG = {
    signature: cfg.reddit.signature
  }
}

const isMissing = R.either(R.isNil, R.isEmpty)
const isNotMissing = R.pipe(isMissing, R.not)

const spreadCancer = () => cancer[Math.floor(Math.random() * cancer.length)] + '  \n\n'

const getQuip = () => quips[Math.floor(Math.random() * quips.length)] + '  \n\n'

const pageHasAtLeastOneJourno = R.anyPass([
  R.propSatisfies(isNotMissing, 'authors'),
  R.propSatisfies(isNotMissing, 'fromTitle'),
  R.propSatisfies(isNotMissing, 'fromBody')
])

export function findJournos (type, journoDB, document) {
  let fields = searchFields[type]
  if (R.isNil(fields)) {
    throw Error('FATAL: unknown document type')
  }
  let page = {
    authors: [],
    fromTitle: [],
    fromBody: []
  }
  let allJournos = []
  for (let journo of journoDB) {
    for (let field of fields) {
      if (R.isNil(document[field])) {
        continue
      }
      let ltext = R.toLower(document[field])
      let lterm = R.toLower(journo.searchTerm)
      let idx = ltext.indexOf(lterm)
      if (idx !== -1) {
        if (type === 'webpage') {
          if (R.isNil(page.title)) {
            page.title = document.title
            page.hostname = urllib.parse(document.url).hostname
            page.url = document.origin
          }
          if (R.contains(journo.name, allJournos)) {
            continue
          }
          allJournos.push(journo.name)
        } else if (type === 'post') {
          if (R.isNil(page.title)) {
            page.title = 'the OP'
            page.hostname = null
            page.url = null
          }
          if (R.contains(journo.name, allJournos)) {
            continue
          }
          allJournos.push(journo.name)
        } else {
          throw new Error(`Unknown document type "${type}"`)
        }
        let tgt
        if (field === 'author') {
          tgt = page.authors
        } else if (field === 'title') {
          tgt = page.fromTitle
        } else if (field === 'selftext' || field === 'content') {
          tgt = page.fromBody
        } else {
          throw new Error(`Unknown field "${field}"`)
        }
        tgt.push({
          name: journo.name,
          link: journo.url
        })
      }
    }
  }
  if (pageHasAtLeastOneJourno(page)) {
    return page
  } else {
    return null
  }
}

function mdWebpageLink ({ title, hostname, url }) {
  if (R.isNil(url)) {
    return title
  } else {
    return `["*${title}*" (from ${hostname})](${url})`
  }
}

function mdWebpageMention ({ title, hostname, url, where }) {
  let mdLink = mdWebpageLink({ title, hostname, url })
  return where + mdLink
}

function mdJournoLink ({ name, link }) {
  return `**[${name}](${link})**`
}

function mdList (list, conjunction = ' and ', separator = ', ') {
  if (isMissing(list)) {
    return ''
  }
  let output = ''
  for (let i = 0, l = list.length; i < l; ++i) {
    if (l > 1 && i === l - 2) {
      output += list[i] + conjunction
    } else if (i === l - 1) {
      output += list[i]
    } else {
      output += list[i] + separator
    }
  }
  return output
}

function mdSentenceByArticle (data) {
  let content = mdWebpageLink(data)
  if (isNotMissing(data.authors)) {
    content += ' was written by ' + mdList(R.map(mdJournoLink, data.authors))
    if (isNotMissing(data.fromTitle) || isNotMissing(data.fromBody)) {
      content += '; it'
    } else {
      content += '.'
    }
  }
  if (isNotMissing(data.fromTitle)) {
    content += ' mentions ' + mdList(R.map(mdJournoLink, data.fromTitle)) + ' in the title'
    if (isNotMissing(data.fromBody)) {
      content += ' and '
    } else {
      content += '.'
    }
  }
  if (isNotMissing(data.fromBody)) {
    if (isMissing(data.fromTitle)) {
      content += ' mentions '
    }
    content += mdList(R.map(mdJournoLink, data.fromBody)) + ' in its body.'
  }
  return content
}

function mdSentenceByJourno ({ name, link, authorOf, mentionedIn }) {
  let content = mdJournoLink({ name, link })
  if (isNotMissing(authorOf)) {
    content += ' is the author of ' + mdList(R.map(mdWebpageLink, authorOf))
    if (isNotMissing(mentionedIn)) {
      content += '; xir\'s '
    } else {
      content += '.'
    }
  } else {
    content += ' is '
  }
  if (isNotMissing(mentionedIn)) {
    content += 'mentioned in ' + mdList(R.map(mdWebpageMention, mentionedIn)) + '.'
  }
  return content
}

export function composeReply (_pages, context = { subreddit: '' }) {
  let pages = R.clone(_pages)
  if (isMissing(pages)) {
    return ''
  }
  let journos = {}
  const init = (j) => (journos[j.name] = {
    name: j.name,
    link: j.link,
    mentions: 0,
    authorOf: [],
    mentionedIn: []
  })
  const scrub = (name) => {
    for (let page of pages) {
      page.authors = R.reject(R.propEq('name', name), page.authors)
      page.fromTitle = R.reject(R.propEq('name', name), page.fromTitle)
      page.fromBody = R.reject(R.propEq('name', name), page.fromBody)
    }
  }
  for (let page of pages) {
    for (let author of page.authors) {
      if (R.isNil(journos[author.name])) {
        init(author)
      }
      journos[author.name].mentions++
      journos[author.name].authorOf.push({
        title: page.title,
        hostname: page.hostname,
        url: page.url
      })
    }
    for (let inTitle of page.fromTitle) {
      if (R.isNil(journos[inTitle.name])) {
        init(inTitle)
      }
      journos[inTitle.name].mentions++
      journos[inTitle.name].mentionedIn.push({
        title: page.title,
        hostname: page.hostname,
        url: page.url,
        where: 'the title of '
      })
    }
    for (let inBody of page.fromBody) {
      if (R.isNil(journos[inBody.name])) {
        init(inBody)
      }
      journos[inBody.name].mentions++
      journos[inBody.name].mentionedIn.push({
        title: page.title,
        hostname: page.hostname,
        url: page.url,
        where: 'the body of '
      })
    }
  }
  journos = R.filter(R.propSatisfies(R.lt(1), 'mentions'))(Object.values(journos))
  let content = []
  if (isNotMissing(journos)) {
    for (let journo of journos) {
      scrub(journo.name)
      content.push(mdSentenceByJourno(journo))
    }
    pages = R.filter(R.anyPass([
      R.propSatisfies(isNotMissing, 'authors'),
      R.propSatisfies(isNotMissing, 'fromTitle'),
      R.propSatisfies(isNotMissing, 'fromBody')
    ]))(pages)
  }
  if (isNotMissing(pages)) {
    for (let page of pages) {
      let newContent = mdSentenceByArticle(page)
      newContent = newContent.charAt(0).toUpperCase() + newContent.slice(1)
      content.push(newContent)
    }
  }
  let signature = R.replace(/%SUBREDDIT%/g, context.subreddit, CFG.signature)
  return mustache.render(replyTemplate, { header: spreadCancer(), quip: getQuip(), content: content.join('\n\n'), footer: signature })
}

export const API = { reconfigure, findJournos, composeReply }

export default API
