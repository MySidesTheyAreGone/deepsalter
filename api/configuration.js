/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'

import { version } from '../package.json'
const defaultUserAgent = `Node/${process.version} Deepsalter/v${version}`

const isMissing = R.either(R.isNil, R.isEmpty)

const defaultConfiguration = {
  deepfreeze: {
    endpoint: 'http://deepfreeze.it/api/index.php?journos=guzo&m=json',
    journoPageBaseURL: 'http://deepfreeze.it/journo.php?j=',
    TTL: 24
  },
  reddit: {
    clientId: null,
    clientSecret: null,
    username: null,
    password: null,
    authUrl: 'https://www.reddit.com/api/v1/access_token',
    apiBaseUrl: 'https://oauth.reddit.com/',
    tokenExpiry: 55, // minutes
    subreddits: [],
    limit: 20,
    userAgent: defaultUserAgent,
    delay: 100,
    concurrency: 2,
    budget: 60,
    budgetDuration: 61000,
    signature: '  \n\nDeepfreeze profiles are historical records ([**read more**](http://deepfreeze.it/about.php)). They are neither a condemnation nor an endorsement.  \n[**[bot issues]**](https://gitgud.io/MySidesTheyAreGone/deepsalter/issues)'
  },
  scraper: {
    userAgent: defaultUserAgent,
    maxDownloadSize: 4000000,
    delay: 100,
    concurrency: 2,
    budget: Infinity,
    budgetDuration: Infinity
  }
}

function splitKeys (usercfg) {
  let newcfg = {}
  for (let k of R.keys(usercfg)) {
    let lens
    if (R.contains('_', k)) {
      lens = R.lensPath(R.split('_', k))
    } else {
      lens = R.lensProp(k)
    }
    newcfg = R.set(lens, usercfg[k], newcfg)
  }
  return newcfg
}

export function applyUserConfiguration (configuration) {
  let parsedCFG = splitKeys(configuration)
  if (!isMissing(parsedCFG.reddit.subreddits)) {
    parsedCFG.reddit.subreddits = R.split(',', parsedCFG.reddit.subreddits)
  }
  if (!isMissing(parsedCFG.reddit.signature)) {
    parsedCFG.reddit.signature = R.replace(/\\n/g, '\n', parsedCFG.reddit.signature)
  }
  return R.mergeDeepRight(defaultConfiguration, parsedCFG)
}

export const ENV_MASK = [
  'deepfreeze_endpoint',
  'deepfreeze_journoPageBaseURL',
  'deepfreeze_TTL',
  'reddit_clientId',
  'reddit_clientSecret',
  'reddit_username',
  'reddit_password',
  'reddit_authUrl',
  'reddit_apiBaseUrl',
  'reddit_tokenExpiry',
  'reddit_subreddits',
  'reddit_limit',
  'reddit_userAgent',
  'reddit_delay',
  'reddit_concurrency',
  'reddit_budget',
  'reddit_budgetDuration',
  'reddit_signature',
  'scraper_userAgent',
  'scraper_maxDownloadSize',
  'scraper_delay',
  'scraper_concurrency',
  'scraper_budget',
  'scraper_budgetDuration'
]

export default applyUserConfiguration
