// adapted from https://www.npmjs.com/package/cloudscraper
// I removed anything tying it to `request` in order to use `got` instead

import R from 'ramda'
import urllib from 'url'
import vm from 'vm'

const isMissing = R.either(R.isNil, R.isEmpty)

function throwCloudflareError (body) {
  if (body.indexOf('why_captcha') !== -1 || /cdn-cgi\/l\/chk_captcha/i.test(body)) {
    throw new Error('Cloudflare returned a captcha challenge')
  }
  let match = body.match(/<\w+\s+class="cf-error-code">(.*)<\/\w+>/i)
  if (!isMissing(match)) {
    throw new Error('Cloudflare returned error ' + parseInt(match[1]))
  }
}

const extractChallengeMethodBody = R.pipe(
  R.nth(1),
  R.replace(/a\.value =(.+?) \+ .+?;/i, '$1'),
  R.replace(/\s{3,}[a-z](?: = |\.).+/g, ''),
  R.replace(/'; \d+'/g, '')
)

function solveMathChallenge (options, response, body) {
  let challenge = body.match(/name="jschl_vc" value="(\w+)"/)
  if (isMissing(challenge)) {
    throw new Error('Cloudflare answered with an challenge I cannot solve: cannot extract challengeId (jschl_vc) from page')
  }
  let jsChlVc = challenge[1]
  challenge = body.match(/getElementById\('cf-content'\)[\s\S]+?setTimeout.+?\r?\n([\s\S]+?a\.value =.+?)\r?\n/i)
  if (isMissing(challenge)) {
    throw new Error('Cloudflare answered with an challenge I cannot solve: cannot extract method body from setTimeOut wrapper')
  }
  let challengePass = body.match(/name="pass" value="(.+?)"/)[1]
  let challengeMethodBody = extractChallengeMethodBody(challenge)
  let urlInfo = urllib.parse(options.url)
  let challengeAnswer
  try {
    challengeAnswer = eval(challengeMethodBody) + urlInfo.host.length // eslint-disable-line
  } catch (e) {
    throw new Error('While evaluating the challenge served by Cloudflare: ' + e.message)
  }
  let answerResponse = {
    'jschl_vc': jsChlVc,
    'pass': challengePass,
    'jschl_answer': challengeAnswer
  }
  let answerUrl = urlInfo.protocol + '//' + urlInfo.host + '/cdn-cgi/l/chk_jschl'

  options.headers['Referer'] = options.url
  options.url = answerUrl
  options.query = answerResponse

  return options
}

function solveCookieChallenge (options, response, body) {
  let challenge = body.match(/S='([^']+)'/)
  if (isMissing(challenge)) {
    throw Error('Cannot extract cookie generation code from Cloudflare page')
  }
  var base64EncodedCode = challenge[1]
  var cookieSettingCode = Buffer.from(base64EncodedCode, 'base64').toString('ascii')
  var sandbox = {
    location: {
      reload: function () {}
    },
    document: {}
  }
  try {
    vm.runInNewContext(cookieSettingCode, sandbox)
  } catch (e) {
    throw new Error('While evaluating the cookie challenge served by Cloudflare: ' + e.message)
  }
  options.headers.cookie = sandbox.document.cookie
  return options
}

export function getProtection (body) {
  if (body.indexOf('a = document.getElementById(\'jschl-answer\');') !== -1) {
    return 'challenge'
  } else if (body.indexOf('You are being redirected') !== -1 || body.indexOf('sucuri_cloudproxy_js') !== -1) {
    return 'redirection'
  } else {
    return false
  }
}

export function deprotect (options, response) {
  if (!response.cloudflare) {
    return { action: 'done', payload: response }
  }
  let body = response.body
  throwCloudflareError(body)
  let protection = getProtection(body)
  if (protection === 'challenge') {
    return { action: 'delayed request', payload: solveMathChallenge(options, response, body) }
  }
  if (protection === 'redirection') {
    return { action: 'request', payload: solveCookieChallenge(options, response, body) }
  }
}

export function reconfigure () {}

export const cloudflareDelay = 6 // seconds

export const API = { getProtection, deprotect, cloudflareDelay }

export default API
