/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import * as actionTypes from './actionTypes.js'

const simple = type => () => ({ type })
const withData = type => data => ({ type, data })
const withError = type => error => ({ type, error })

export const updateToken = simple(actionTypes.REDDIT_UPDATE_TOKEN)
export const updateTokenOK = withData(actionTypes.REDDIT_UPDATE_TOKEN_OK)
export const updateTokenKO = withError(actionTypes.REDDIT_UPDATE_TOKEN_KO)

export const fetchPosts = simple(actionTypes.REDDIT_FETCH_POSTS)
export const fetchPostsOK = withData(actionTypes.REDDIT_FETCH_POSTS_OK)
export const fetchPostsKO = withError(actionTypes.REDDIT_FETCH_POSTS_KO)

export const savePost = withData(actionTypes.REDDIT_SAVE_POST)
export const savePostOK = withData(actionTypes.REDDIT_SAVE_POST_OK)
export const savePostKO = withError(actionTypes.REDDIT_SAVE_POST_KO)
export const savePostSkipped = withData(actionTypes.REDDIT_SAVE_POST_SKIPPED)

export const replyPost = withData(actionTypes.REDDIT_REPLY_POST)
export const replyPostOK = withData(actionTypes.REDDIT_REPLY_POST_OK)
export const replyPostUnnecessary = withData(actionTypes.REDDIT_REPLY_POST_UNNECESSARY)
export const replyPostKO = withError(actionTypes.REDDIT_REPLY_POST_KO)
export const replyPostSkipped = withData(actionTypes.REDDIT_REPLY_POST_SKIPPED)

export const purge = simple(actionTypes.REDDIT_PURGE)

export const actions = {
  updateToken,
  updateTokenOK,
  updateTokenKO,
  fetchPosts,
  fetchPostsOK,
  fetchPostsKO,
  savePost,
  savePostOK,
  savePostKO,
  savePostSkipped,
  replyPost,
  replyPostOK,
  replyPostKO,
  replyPostSkipped,
  replyPostUnnecessary,
  purge
}

export default actions
