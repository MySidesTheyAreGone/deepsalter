/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation,
either version 3 of the License,
or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not,
see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

export const SYSTEM_LOG = 'SYSTEM_LOG'

export const DEEPFREEZE_UPDATE = 'DEEPFREEZE_UPDATE'
export const DEEPFREEZE_UPDATE_OK = 'DEEPFREEZE_UPDATE_OK'
export const DEEPFREEZE_UPDATE_KO = 'DEEPFREEZE_UPDATE_KO'
export const DEEPFREEZE_UPDATE_SKIPPED = 'DEEPFREEZE_UPDATE_SKIPPED'

export const REDDIT_UPDATE_TOKEN = 'REDDIT_UPDATE_TOKEN'
export const REDDIT_UPDATE_TOKEN_OK = 'REDDIT_UPDATE_TOKEN_OK'
export const REDDIT_UPDATE_TOKEN_KO = 'REDDIT_UPDATE_TOKEN_KO'

export const REDDIT_FETCH_POSTS = 'REDDIT_FETCH_POSTS'
export const REDDIT_FETCH_POSTS_OK = 'REDDIT_FETCH_POSTS_OK'
export const REDDIT_FETCH_POSTS_KO = 'REDDIT_FETCH_POSTS_KO'

export const REDDIT_SAVE_POST = 'REDDIT_SAVE_POST'
export const REDDIT_SAVE_POST_OK = 'REDDIT_SAVE_POST_OK'
export const REDDIT_SAVE_POST_KO = 'REDDIT_SAVE_POST_KO'
export const REDDIT_SAVE_POST_SKIPPED = 'REDDIT_SAVE_POST_SKIPPED'

export const REDDIT_REPLY_POST = 'REDDIT_REPLY_POST'
export const REDDIT_REPLY_POST_OK = 'REDDIT_REPLY_POST_OK'
export const REDDIT_REPLY_POST_KO = 'REDDIT_REPLY_POST_KO'
export const REDDIT_REPLY_POST_SKIPPED = 'REDDIT_REPLY_POST_SKIPPED'
export const REDDIT_REPLY_POST_UNNECESSARY = 'REDDIT_REPLY_POST_UNNECESSARY'

export const REDDIT_PURGE = 'REDDIT_PURGE'

export const SCRAPE_POST = 'REDDIT_SCRAPE_POST'
export const SCRAPE_POST_OK = 'REDDIT_SCRAPE_POST_OK'
export const SCRAPE_POST_KO = 'REDDIT_SCRAPE_POST_KO'
export const SCRAPE_POST_SKIPPED = 'SCRAPE_POST_SKIPPED'

export const DEEPSALTER_INC_SAVED = 'DEEPSALTER_INC_SAVED'
export const DEEPSALTER_INC_REPLIED = 'DEEPSALTER_INC_REPLIED'
export const DEEPSALTER_INC_JOURNOS = 'DEEPSALTER_INC_JOURNOS'

export const actions = {
  SYSTEM_LOG,
  DEEPFREEZE_UPDATE,
  DEEPFREEZE_UPDATE_KO,
  DEEPFREEZE_UPDATE_OK,
  DEEPFREEZE_UPDATE_SKIPPED,
  REDDIT_UPDATE_TOKEN,
  REDDIT_UPDATE_TOKEN_KO,
  REDDIT_UPDATE_TOKEN_OK,
  REDDIT_FETCH_POSTS,
  REDDIT_FETCH_POSTS_KO,
  REDDIT_FETCH_POSTS_OK,
  REDDIT_SAVE_POST,
  REDDIT_SAVE_POST_KO,
  REDDIT_SAVE_POST_OK,
  REDDIT_SAVE_POST_SKIPPED,
  REDDIT_REPLY_POST,
  REDDIT_REPLY_POST_KO,
  REDDIT_REPLY_POST_OK,
  REDDIT_REPLY_POST_SKIPPED,
  REDDIT_REPLY_POST_UNNECESSARY,
  REDDIT_PURGE,
  SCRAPE_POST,
  SCRAPE_POST_OK,
  SCRAPE_POST_KO,
  SCRAPE_POST_SKIPPED,
  DEEPSALTER_INC_SAVED,
  DEEPSALTER_INC_REPLIED,
  DEEPSALTER_INC_JOURNOS
}

export default actions
