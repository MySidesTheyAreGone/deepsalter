/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import { take, call, put, select } from 'redux-saga/effects'
import selectors from './selectors.js'
import * as actionTypes from '../actions/actionTypes.js'
import acts from '../actions/index.js'

const delay = s => new Promise(resolve => setTimeout(resolve, s * 1000))

export function create (API) {
  function * mainLoop () {
    while (true) {
      yield call(delay, 7)

      yield put(acts.deepfreeze.update())
      yield take([
        actionTypes.DEEPFREEZE_UPDATE_OK,
        actionTypes.DEEPFREEZE_UPDATE_KO,
        actionTypes.DEEPFREEZE_UPDATE_SKIPPED
      ])

      yield put(acts.reddit.fetchPosts())
      let result = yield take([
        actionTypes.REDDIT_FETCH_POSTS_OK,
        actionTypes.REDDIT_FETCH_POSTS_KO
      ])
      if (result.type === actionTypes.REDDIT_FETCH_POSTS_KO) {
        continue
      }

      let posts = yield select(selectors.getRedditPosts)
      for (let post of posts) {
        yield put(acts.scraper.scrapePost(post))
        let result = yield take([
          actionTypes.SCRAPE_POST_OK,
          actionTypes.SCRAPE_POST_KO,
          actionTypes.SCRAPE_POST_SKIPPED
        ])
        if (result.type === actionTypes.SCRAPE_POST_KO) {
          continue
        }
        let updatedPost = yield select(selectors.getRedditPost(result.data.id))
        yield put(acts.reddit.replyPost(updatedPost))
        result = yield take([
          actionTypes.REDDIT_REPLY_POST_OK,
          actionTypes.REDDIT_REPLY_POST_KO,
          actionTypes.REDDIT_REPLY_POST_SKIPPED,
          actionTypes.REDDIT_REPLY_POST_UNNECESSARY
        ])
        if (result.type === actionTypes.REDDIT_REPLY_POST_KO) {
          continue
        }
        if (result.type !== actionTypes.REDDIT_REPLY_POST_SKIPPED && result.type !== actionTypes.REDDIT_REPLY_POST_UNNECESSARY) {
          yield put(acts.deepsalter.incReplied())
        }
        updatedPost = yield select(selectors.getRedditPost(result.data.id))
        let names = []
        for (let pageData of updatedPost.journos) {
          names = R.concat(names, R.pluck('name', pageData.authors))
          names = R.concat(names, R.pluck('name', pageData.fromTitle))
          names = R.concat(names, R.pluck('name', pageData.fromBody))
        }
        names = R.uniq(names)
        yield put(acts.deepsalter.incJournos(names.length))
        yield put(acts.reddit.savePost(updatedPost))
        result = yield take([
          actionTypes.REDDIT_SAVE_POST_OK,
          actionTypes.REDDIT_SAVE_POST_KO,
          actionTypes.REDDIT_SAVE_POST_SKIPPED
        ])
        if (result.type === actionTypes.REDDIT_SAVE_POST_KO) {
          continue
        }
        if (result.type !== actionTypes.REDDIT_SAVE_POST_SKIPPED) {
          yield put(acts.deepsalter.incSaved())
        }
      }
      yield put(acts.reddit.purge())
    }
  }

  return { startup: [mainLoop] }
}

export default create
