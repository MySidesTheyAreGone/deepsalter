/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import moment from 'moment'
import { take, call, put, select } from 'redux-saga/effects'
import selectors from './selectors.js'
import { DEEPFREEZE_UPDATE } from '../actions/actionTypes.js'
import acts from '../actions/index.js'

export function create (API) {
  function * updateDeepfreezeDatabase () {
    while (true) {
      yield take(DEEPFREEZE_UPDATE)
      const expiration = yield select(selectors.getDeepfreezeExpirationDate)
      if (R.isNil(expiration) || expiration.isSameOrBefore(moment())) {
        try {
          yield put(acts.system.log('info', 'Refreshing Deepfreeze DB'))
          const data = yield call(API.deepfreeze.fetch)
          yield put(acts.deepfreeze.updateOK(data))
        } catch (e) {
          yield put(acts.system.log('error', `I failed to update Deepfreeze's database. This error is not fatal.`))
          yield put(acts.system.log('error', e))
          yield put(acts.deepfreeze.updateKO(e))
        }
      } else {
        yield put(acts.deepfreeze.updateSkipped())
      }
    }
  }

  return { updateDeepfreezeDatabase, startup: [updateDeepfreezeDatabase] }
}

export default create
