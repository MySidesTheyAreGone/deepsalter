/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import { take, call, put, select } from 'redux-saga/effects'
import selectors from './selectors.js'
import * as actionTypes from '../actions/actionTypes.js'
import acts from '../actions/index.js'

const isMissing = R.either(R.isNil, R.isEmpty)

export function create (API) {
  function * scrapePosts () {
    while (true) {
      let post = R.prop('data', yield take(actionTypes.SCRAPE_POST))
      if (post.scraped) {
        yield put(acts.system.log('info', `Skipped scraping ${post.id}: post already scraped`))
        yield put(acts.scraper.scrapePostSkipped(post))
        continue
      }
      let dfdb = yield select(selectors.getDeepfreezeDatabase)
      let journos = R.reject(R.isNil, R.of(yield call(API.documents.findJournos, 'post', dfdb, post)))
      for (let url of post.externalResources) {
        let document
        let attempts = 0
        let errors = []
        let hasFailure = true
        while (++attempts <= 3 && hasFailure) {
          try {
            document = yield call(API.web.scrape, url)
          } catch (e) {
            errors.push(e)
            continue
          }
          hasFailure = false
        }
        if (!hasFailure) {
          let pageData = yield call(API.documents.findJournos, 'webpage', dfdb, document)
          if (!isMissing(pageData)) {
            journos.push(pageData)
          }
        } else {
          yield put(acts.system.log('error', `I failed to scrape ${url}`))
          for (let e of errors) {
            yield put(acts.system.log('error', e))
          }
        }
      }
      let replyText = API.documents.composeReply(journos, { subreddit: post.subreddit })
      yield put(acts.scraper.scrapePostOK({ id: post.id, journos, replyText }))
    }
  }

  return { scrapePosts, startup: [scrapePosts] }
}

export default create
