/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import moment from 'moment'

import { take, call, put, select } from 'redux-saga/effects'
import selectors from './selectors.js'
import * as actionTypes from '../actions/actionTypes.js'
import acts from '../actions/index.js'

export function create (API) {
  function * updateToken () {
    let token
    while (true) {
      yield take(actionTypes.REDDIT_UPDATE_TOKEN)
      try {
        yield put(acts.system.log('info', 'Updating reddit authentication token'))
        token = yield call(API.reddit.getAuthToken)
      } catch (e) {
        yield put(acts.reddit.updateTokenKO(e))
      }
      yield put(acts.reddit.updateTokenOK(token))
      token = null
    }
  }

  function * getAccessToken () {
    let token = yield select(selectors.getRedditToken)
    if (R.isNil(token) || token.expirationDate.isSameOrBefore(moment())) {
      yield put(acts.reddit.updateToken())
      let result = yield take([
        actionTypes.REDDIT_UPDATE_TOKEN_OK,
        actionTypes.REDDIT_UPDATE_TOKEN_KO
      ])
      if (result.type === actionTypes.REDDIT_UPDATE_TOKEN_KO) {
        yield put(acts.system.log('error', 'I failed to update reddit\'s auth token'))
        yield put(acts.system.log('error', result.error))
        return null
      } else {
        token = result.data
      }
    }
    return token
  }

  function * fetchPosts () {
    while (true) {
      yield take(actionTypes.REDDIT_FETCH_POSTS)
      let token = yield call(getAccessToken)
      if (R.isNil(token)) {
        yield put(acts.reddit.fetchPostsKO(new Error('Cannot fetch new posts: missing auth token')))
      }
      let fetched
      try {
        fetched = yield call(API.reddit.getNewPosts, token)
      } catch (e) {
        yield put(acts.system.log('error', `I failed to fetch the latest posts from reddit`))
        yield put(acts.system.log('error', e))
        yield put(acts.reddit.fetchPostsKO(e))
        continue
      }
      yield put(acts.reddit.fetchPostsOK(fetched))
    }
  }

  function * savePosts () {
    while (true) {
      let post = R.prop('data', yield take(actionTypes.REDDIT_SAVE_POST))
      let token = yield call(getAccessToken)
      if (R.isNil(token)) {
        yield put(acts.reddit.savePostKO(new Error('Missing auth token')))
      }
      if (post.saved) {
        yield put(acts.system.log('info', `Skipped saving ${post.id}: post already saved`))
        yield put(acts.reddit.savePostSkipped(post))
        continue
      }
      try {
        yield put(acts.system.log('info', `Saving post ${post.id}...`))
        yield call(API.reddit.savePost, token, post)
      } catch (e) {
        yield put(acts.system.log('error', `I failed to save post ${post.id}`))
        yield put(acts.system.log('error', e))
        yield put(acts.reddit.savePostKO(e))
        continue
      }
      yield put(acts.reddit.savePostOK(post))
    }
  }

  function * replyToPosts () {
    while (true) {
      let post = R.prop('data', yield take(actionTypes.REDDIT_REPLY_POST))
      let token = yield call(getAccessToken)
      if (R.isNil(token)) {
        yield put(acts.reddit.replyPostKO(new Error('Missing auth token')))
      }
      if (R.isEmpty(post.journos)) {
        yield put(acts.system.log('info', `Skipped replying to ${post.id}: no journos found`))
        yield put(acts.reddit.replyPostUnnecessary(post))
        continue
      }
      if (post.replied) {
        yield put(acts.system.log('info', `Skipped replying to ${post.id}: ${post.replied ? 'replied to it earlier' : 'no journos found'}`))
        yield put(acts.reddit.replyPostSkipped(post))
        continue
      }
      try {
        yield put(acts.system.log('info', `Replying to post ${post.id}...`))
        yield call(API.reddit.replyToThing, token, post)
      } catch (e) {
        yield put(acts.system.log('error', `I failed replying to post ${post.id}`))
        yield put(acts.system.log('error', e))
        yield put(acts.reddit.replyPostKO(e))
        continue
      }
      yield put(acts.reddit.replyPostOK(post))
    }
  }

  return { fetchPosts, savePosts, replyToPosts, updateToken, startup: [fetchPosts, savePosts, replyToPosts, updateToken] }
}

export default create
