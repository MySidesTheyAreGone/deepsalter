/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'

export const getDeepfreezeExpirationDate = R.path(['deepfreeze', 'expirationDate'])
export const getDeepfreezeDatabase = R.path(['deepfreeze', 'db'])
export const getRedditToken = R.path(['reddit', 'token'])
export const getRedditPosts = R.path(['reddit', 'posts'])
export const getRedditPost = (id) => R.pipe(R.path(['reddit', 'posts']), R.find(R.propEq('id', id)))

export const selectors = {
  getDeepfreezeDatabase,
  getDeepfreezeExpirationDate,
  getRedditToken,
  getRedditPosts,
  getRedditPost
}

export default selectors
