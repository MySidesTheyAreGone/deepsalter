
import moment from 'moment'
import * as actionTypes from '../actions/actionTypes.js'

const initialState = {
  saved: 0,
  replied: 0,
  journos: 0,
  since: moment().format('DD/MM/YYYY HH:mm:ss')
}

const reducer = {}

reducer[actionTypes.DEEPSALTER_INC_SAVED] = (state, action) => {
  return {
    ...state,
    saved: state.saved + 1
  }
}

reducer[actionTypes.DEEPSALTER_INC_REPLIED] = (state, action) => {
  return {
    ...state,
    replied: state.replied + 1
  }
}

reducer[actionTypes.DEEPSALTER_INC_JOURNOS] = (state, action) => {
  return {
    ...state,
    journos: state.journos + action.data
  }
}

export const rootReducer = (state = initialState, action) => {
  if (reducer[action.type] == null) {
    return state
  }
  return reducer[action.type](state, action)
}

export default rootReducer
