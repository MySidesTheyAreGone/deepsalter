/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import * as actionTypes from '../actions/actionTypes.js'
import moment from 'moment'
import R from 'ramda'

const initialState = {
  lastMessages: []
}

const reducer = {}

reducer[actionTypes.SYSTEM_LOG] = (state, action) => {
  const { level, message } = action
  const ts = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss.SSS')
  const msg = `${ts} [${level}] ${message}`
  return {
    ...state,
    lastMessages: R.pipe(R.flip(R.append)(state.lastMessages), R.take(100))(msg)
  }
}

const rootReducer = (state = initialState, action) => {
  if (reducer[action.type] == null) {
    return state
  }
  return reducer[action.type](state, action)
}

export default rootReducer
