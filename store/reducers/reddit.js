/*
  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `

    Deepsalter - Trust should be earned
    Copyright (C) 2016-2018 MySidesTheyAreGone <mysidestheyaregone@protonmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

  .--.      .-'.      .--.      .--.      .--.      .--.      .`-.      .--.
:::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\::::::::.\
'      `--'      `.-'      `--'      `--'      `--'      `-.'      `--'      `
*/

import R from 'ramda'
import * as actionTypes from '../actions/actionTypes.js'

const initialState = {
  posts: [],
  token: null
}

const reducer = {}

reducer[actionTypes.REDDIT_UPDATE_TOKEN_OK] = (state, action) => {
  return {
    ...state,
    token: action.data
  }
}

const processNewPosts = R.pipe(
  R.filter(R.propEq('saved', false)),
  R.map(R.pipe(
    R.pick(['name', 'id', 'saved', 'title', 'url', 'is_self', 'selftext', 'externalResources', 'subreddit']),
    R.assoc('replied', false),
    R.assoc('scraped', false),
    R.assoc('journos', []),
    R.assoc('replyText', null)
  ))
)

reducer[actionTypes.REDDIT_FETCH_POSTS_OK] = (state, action) => {
  const maybeNewPosts = processNewPosts(action.data)
  let oldposts = R.defaultTo([], state.posts)
  const previouslyFetchedIds = R.pluck('id', oldposts)
  const neverSeenBefore = R.reject(R.pipe(
    R.prop('id'),
    R.flip(R.contains)(previouslyFetchedIds)
  ), maybeNewPosts)
  const brandNew = R.concat(oldposts, neverSeenBefore)

  return {
    ...state,
    fetchingPosts: false,
    thereAreNewPosts: brandNew.length > 0,
    posts: brandNew
  }
}

reducer[actionTypes.REDDIT_SAVE_POST_OK] = (state, action) => {
  let id = action.data.id
  return {
    ...state,
    posts: R.map(R.ifElse(
      R.propEq('id', id),
      R.assoc('saved', true),
      R.identity
    ), state.posts)
  }
}

reducer[actionTypes.SCRAPE_POST_OK] = (state, action) => {
  let post = action.data
  return {
    ...state,
    posts: R.map(R.ifElse(
      R.propEq('id', post.id),
      R.pipe(
        R.assoc('journos', post.journos),
        R.assoc('replyText', post.replyText),
        R.assoc('scraped', true)
      ),
      R.identity
    ), state.posts)
  }
}

reducer[actionTypes.REDDIT_REPLY_POST_UNNECESSARY] =
reducer[actionTypes.REDDIT_REPLY_POST_OK] = (state, action) => {
  let id = action.data.id
  return {
    ...state,
    posts: R.map(R.ifElse(
      R.propEq('id', id),
      R.assoc('replied', true),
      R.identity
    ), state.posts)
  }
}

reducer[actionTypes.REDDIT_PURGE] = (state, action) => {
  return {
    ...state,
    posts: R.reject(R.allPass([
      R.propEq('scraped', true),
      R.propEq('replied', true),
      R.propEq('saved', true)
    ]), state.posts)
  }
}

export const rootReducer = (state = initialState, action) => {
  if (reducer[action.type] == null) {
    return state
  }
  return reducer[action.type](state, action)
}

export default rootReducer
